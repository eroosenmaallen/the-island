# The Island of Misfit Projects

This repo is intended as a place to put down your unfinished projects, to be picked up by someone whose brain-squirrels won't let them start anything new.


## Guiding principles

Nothing is apolitical, so here is my political baseline: this is a space for marginalized people, LGBT, BIPOC, neurodiverse, etc., to work together. We are all human.


## Getting started

To make it easy for you to get started with The Island, here's a list of recommended next steps.


## Add your projects

- [ ] Create a directory `contributors/your-screen-name`
- [ ] Introduce yourself in `contributors/your-screen-name/README.md`
- [ ] ...? (tbd: Recommended arrangement for projects under a contributor directory)
- [ ] ...? (tbd: Recommended arrangement for a Directory of projects, organized by category in some way)
- [ ] Profit!

