# Sending projects to The Island

We use the usual fork-modify-MR process, where you fork your own copy of the repo, work on your own branch, then open an MR to merge your changes back into `main`.


## Introduce Yourself

First, introduce yourself with a README in `contributors/your-gitlab-username`. In particular, who are you, what sorts of things do you make, and how should people contact you about your abandoned projects?


## Put down projects

For each project you list here, add a directory at `contributors/your-gitlab-username/project-slug` with a README that contains a summary of your project and a link back to it.

Find the right place(s) in the Directory (to be made) to link to your project


## Pick up projects

See something cool? Reach out to the dev via the repo or other contacts they list in their README. Or just fork it!

