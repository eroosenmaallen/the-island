# eroosenmaallen - Eddie Roosenmaallen

I'm the original organizer of The Island. 

By day I'm the Release Manager at Distributive Corp, building a grid computing platform for science, medicine, and industry. It's pretty cool.

Outside work, I spend time with my family and play with web technologies. I'm queer and genderqueer, AuDHD, and I start many many more things than I finish.

